# WordPress : buffernow.com MySQL database backup
#
# Generated: Wednesday 11. October 2017 23:50 PHT
# Hostname: localhost
# Database: `gradedb`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tbladmin`
# --------------------------------------------------------


#
# Delete any existing table `tbladmin`
#

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `tbladmin`;
SET FOREIGN_KEY_CHECKS=1;


#
# Table structure of table `tbladmin`
#

CREATE TABLE `tbladmin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `accounttype` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ;

#
# Data contents of table tbladmin (1 records)
#
 
INSERT INTO `tbladmin` VALUES (1, 'admin', 'admin', 'Administrator') ;
#
# End of data contents of table tbladmin
# --------------------------------------------------------

# WordPress : buffernow.com MySQL database backup
#
# Generated: Wednesday 11. October 2017 23:50 PHT
# Hostname: localhost
# Database: `gradedb`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tbladmin`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblclass`
# --------------------------------------------------------


#
# Delete any existing table `tblclass`
#

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `tblclass`;
SET FOREIGN_KEY_CHECKS=1;


#
# Table structure of table `tblclass`
#

CREATE TABLE `tblclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classname` varchar(20) NOT NULL,
  `schoolyearid` int(11) NOT NULL,
  `yearlevelid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ;

#
# Data contents of table tblclass (6 records)
#
 
INSERT INTO `tblclass` VALUES (5, 'Dahlia', 4, 4) ; 
INSERT INTO `tblclass` VALUES (6, 'Sampaguita', 4, 5) ; 
INSERT INTO `tblclass` VALUES (7, 'Rose', 4, 6) ; 
INSERT INTO `tblclass` VALUES (8, 'Gumamela', 4, 7) ; 
INSERT INTO `tblclass` VALUES (9, 'Morning Glory', 4, 8) ; 
INSERT INTO `tblclass` VALUES (10, 'Sun Flower', 4, 9) ;
#
# End of data contents of table tblclass
# --------------------------------------------------------

# WordPress : buffernow.com MySQL database backup
#
# Generated: Wednesday 11. October 2017 23:50 PHT
# Hostname: localhost
# Database: `gradedb`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tbladmin`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblschoolyear`
# --------------------------------------------------------


#
# Delete any existing table `tblschoolyear`
#

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `tblschoolyear`;
SET FOREIGN_KEY_CHECKS=1;


#
# Table structure of table `tblschoolyear`
#

CREATE TABLE `tblschoolyear` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schoolyear` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `schoolyear` (`schoolyear`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1 ;

#
# Data contents of table tblschoolyear (2 records)
#
 
INSERT INTO `tblschoolyear` VALUES (24, '2016-2017') ; 
INSERT INTO `tblschoolyear` VALUES (4, '2017-2018') ;
#
# End of data contents of table tblschoolyear
# --------------------------------------------------------

# WordPress : buffernow.com MySQL database backup
#
# Generated: Wednesday 11. October 2017 23:50 PHT
# Hostname: localhost
# Database: `gradedb`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tbladmin`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblschoolyear`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudent`
# --------------------------------------------------------


#
# Delete any existing table `tblstudent`
#

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `tblstudent`;
SET FOREIGN_KEY_CHECKS=1;


#
# Table structure of table `tblstudent`
#

CREATE TABLE `tblstudent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lname` varchar(20) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `mname` varchar(20) NOT NULL,
  `contact` int(11) NOT NULL,
  `address` text NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ;

#
# Data contents of table tblstudent (5 records)
#
 
INSERT INTO `tblstudent` VALUES (2, 'Salamanes', 'Antonio', 'Rocal', 2147483647, '19 - A Gorordo Avenue Lahug, Cebu City', '200506219', '200506219') ; 
INSERT INTO `tblstudent` VALUES (3, 'Sparks', 'Shawn Michael', 'Dano', 1234567890, 'Cebu City Philippines', '19992097', '19992097') ; 
INSERT INTO `tblstudent` VALUES (4, 'Lao', 'Heferson', 'Wew', 1234567890, 'Cebu City Philippines', '2014003756', '2014003756') ; 
INSERT INTO `tblstudent` VALUES (5, 'Bolongaita', 'James', 'O.', 123145161, 'Cebu City Philippines', '19992098', '19992098') ; 
INSERT INTO `tblstudent` VALUES (6, 'Watson', 'Emma', 'W.', 2147483647, 'Cebu City Philippines', '19992099', '19992099') ;
#
# End of data contents of table tblstudent
# --------------------------------------------------------

# WordPress : buffernow.com MySQL database backup
#
# Generated: Wednesday 11. October 2017 23:50 PHT
# Hostname: localhost
# Database: `gradedb`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tbladmin`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblschoolyear`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudent`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudentclass`
# --------------------------------------------------------


#
# Delete any existing table `tblstudentclass`
#

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `tblstudentclass`;
SET FOREIGN_KEY_CHECKS=1;


#
# Table structure of table `tblstudentclass`
#

CREATE TABLE `tblstudentclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(11) NOT NULL,
  `studentid` int(11) NOT NULL,
  `subjectid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `classid` (`classid`),
  KEY `studentid` (`studentid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ;

#
# Data contents of table tblstudentclass (4 records)
#
 
INSERT INTO `tblstudentclass` VALUES (4, 5, 2, 6) ; 
INSERT INTO `tblstudentclass` VALUES (5, 6, 3, 7) ; 
INSERT INTO `tblstudentclass` VALUES (6, 7, 4, 8) ; 
INSERT INTO `tblstudentclass` VALUES (7, 8, 5, 11) ;
#
# End of data contents of table tblstudentclass
# --------------------------------------------------------

# WordPress : buffernow.com MySQL database backup
#
# Generated: Wednesday 11. October 2017 23:50 PHT
# Hostname: localhost
# Database: `gradedb`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tbladmin`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblschoolyear`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudent`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudentclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudentgrade`
# --------------------------------------------------------


#
# Delete any existing table `tblstudentgrade`
#

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `tblstudentgrade`;
SET FOREIGN_KEY_CHECKS=1;


#
# Table structure of table `tblstudentgrade`
#

CREATE TABLE `tblstudentgrade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) NOT NULL,
  `schoolyearid` int(11) NOT NULL,
  `subjectid` int(11) NOT NULL,
  `classid` int(11) NOT NULL,
  `adviserid` int(11) NOT NULL,
  `1stgrading` int(11) NOT NULL,
  `2ndgrading` int(11) NOT NULL,
  `3rdgrading` int(11) NOT NULL,
  `4thgrading` int(11) NOT NULL,
  `gradeaverage` int(11) NOT NULL,
  `remarks` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ;

#
# Data contents of table tblstudentgrade (2 records)
#
 
INSERT INTO `tblstudentgrade` VALUES (1, 1, 4, 1, 3, 1, 23, 23, 45, 4, 24, 'Failed') ; 
INSERT INTO `tblstudentgrade` VALUES (5, 1, 24, 1, 3, 1, 79, 89, 78, 88, 84, 'Passed') ;
#
# End of data contents of table tblstudentgrade
# --------------------------------------------------------

# WordPress : buffernow.com MySQL database backup
#
# Generated: Wednesday 11. October 2017 23:50 PHT
# Hostname: localhost
# Database: `gradedb`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tbladmin`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblschoolyear`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudent`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudentclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudentgrade`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblsubjects`
# --------------------------------------------------------


#
# Delete any existing table `tblsubjects`
#

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `tblsubjects`;
SET FOREIGN_KEY_CHECKS=1;


#
# Table structure of table `tblsubjects`
#

CREATE TABLE `tblsubjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subjectname` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `yearlevelid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ;

#
# Data contents of table tblsubjects (4 records)
#
 
INSERT INTO `tblsubjects` VALUES (6, 'SCIENCE', ' ', 4) ; 
INSERT INTO `tblsubjects` VALUES (7, 'MATHEMATICS', ' ', 5) ; 
INSERT INTO `tblsubjects` VALUES (8, 'HISTORY', ' ', 6) ; 
INSERT INTO `tblsubjects` VALUES (11, 'ENGLISH', ' ', 8) ;
#
# End of data contents of table tblsubjects
# --------------------------------------------------------

# WordPress : buffernow.com MySQL database backup
#
# Generated: Wednesday 11. October 2017 23:50 PHT
# Hostname: localhost
# Database: `gradedb`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tbladmin`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblschoolyear`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudent`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudentclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudentgrade`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblsubjects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblteacher`
# --------------------------------------------------------


#
# Delete any existing table `tblteacher`
#

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `tblteacher`;
SET FOREIGN_KEY_CHECKS=1;


#
# Table structure of table `tblteacher`
#

CREATE TABLE `tblteacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lname` varchar(20) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `mname` varchar(20) NOT NULL,
  `contact` int(11) NOT NULL,
  `address` text NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ;

#
# Data contents of table tblteacher (1 records)
#
 
INSERT INTO `tblteacher` VALUES (2, 'Ybanez', 'Jean', 'P.', 123456789, 'Cebu City Philippines', 'teacher', 'teacher') ;
#
# End of data contents of table tblteacher
# --------------------------------------------------------

# WordPress : buffernow.com MySQL database backup
#
# Generated: Wednesday 11. October 2017 23:50 PHT
# Hostname: localhost
# Database: `gradedb`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tbladmin`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblschoolyear`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudent`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudentclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudentgrade`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblsubjects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblteacher`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblteacheradvisory`
# --------------------------------------------------------


#
# Delete any existing table `tblteacheradvisory`
#

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `tblteacheradvisory`;
SET FOREIGN_KEY_CHECKS=1;


#
# Table structure of table `tblteacheradvisory`
#

CREATE TABLE `tblteacheradvisory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacherid` int(11) NOT NULL,
  `classid` int(11) NOT NULL,
  `subjectid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacherid` (`teacherid`,`classid`),
  KEY `classid` (`classid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ;

#
# Data contents of table tblteacheradvisory (1 records)
#
 
INSERT INTO `tblteacheradvisory` VALUES (3, 2, 9, 6) ;
#
# End of data contents of table tblteacheradvisory
# --------------------------------------------------------

# WordPress : buffernow.com MySQL database backup
#
# Generated: Wednesday 11. October 2017 23:50 PHT
# Hostname: localhost
# Database: `gradedb`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tbladmin`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblschoolyear`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudent`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudentclass`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblstudentgrade`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblsubjects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblteacher`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblteacheradvisory`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `tblyearlevel`
# --------------------------------------------------------


#
# Delete any existing table `tblyearlevel`
#

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `tblyearlevel`;
SET FOREIGN_KEY_CHECKS=1;


#
# Table structure of table `tblyearlevel`
#

CREATE TABLE `tblyearlevel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yearlevel` varchar(20) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ;

#
# Data contents of table tblyearlevel (6 records)
#
 
INSERT INTO `tblyearlevel` VALUES (4, 'Grade 1', 'GRADE SCHOOL (PRIMARY)') ; 
INSERT INTO `tblyearlevel` VALUES (5, 'Grade 2', 'GRADE SCHOOL (PRIMARY)') ; 
INSERT INTO `tblyearlevel` VALUES (6, 'Grade 3', 'GRADE SCHOOL (PRIMARY)') ; 
INSERT INTO `tblyearlevel` VALUES (7, '', 'GRADE SCHOOL (PRIMARY)') ; 
INSERT INTO `tblyearlevel` VALUES (8, 'Grade 5', 'GRADE SCHOOL (PRIMARY)') ; 
INSERT INTO `tblyearlevel` VALUES (9, 'Grade 6', 'GRADE SCHOOL (PRIMARY)') ;
#
# End of data contents of table tblyearlevel
# --------------------------------------------------------

